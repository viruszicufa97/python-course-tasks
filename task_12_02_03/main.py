# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_12_02_03.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


from iihf_analyzer import IihfAnalyzer

if __name__ == "__main__":

    analyzer = IihfAnalyzer()
    try:
        analyzer.load(filename="hockey_players.csv")
        print(analyzer)

        analyzer.show_plot()
    except Exception as err:
        print("Во время работы приложения произошла ошибка:", err)
