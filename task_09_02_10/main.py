# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_09_02_10.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


import sys
import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas
)
import matplotlib.pyplot as plt
from PyQt5.QtWidgets import (
    QApplication,
    QWidget,
    QSizePolicy,
    QVBoxLayout,
    QHBoxLayout,
    QGridLayout,
    QGroupBox,
    QMessageBox,
    QCheckBox,
    QListWidget
)
from PyQt5.QtGui import QIcon
# Масштабирование для 4K-Мониторов
import PyQt5
QApplication.setAttribute(PyQt5.QtCore.Qt.AA_EnableHighDpiScaling, True)
# Удалите комментарий и допишите код

CAPTION_ALL = "Все"


def show_error(message):
    QMessageBox.critical(main_widget, "Ошибка", message)


def show_info(message):
    QMessageBox.information(main_widget, "Информация", message)


def redraw_graph(widget):
    """Отрисовка графика на основании выбранных параметров."""

    try:
        # Тип населения
        if lst_types.currentItem().text() == CAPTION_ALL:
            filter_type = g_bd_types
        else:
            filter_type = {lst_types.currentItem().text()}

        # Годы для анализа
        filter_year = set()
        for rd_year in rd_years:
            if rd_year.isChecked():
                filter_year.add(int(rd_year.text()))

        # Результаты
        # Удалите комментарий и допишите код
        res = # Вызов bd_stats.filter

        # График
        x_labels = [i["Год"] for i in res]
        x_values = list(range(len(x_labels)))
        yr = [i["Родилось"] for i in res]
        yu = [i["Умерло"] for i in res]

        ax.clear()
        ax.set_xlabel("Год")
        ax.set_ylabel("Человек")
        ax.set_xticks(x_values)
        ax.set_xticklabels(x_labels, rotation=45)
        ax.plot(x_values, yr, label="Родилось", marker="*")
        ax.plot(x_values, yu, label="Умерло", marker="*")
        ax.legend(loc="best")
        canvas.draw()

    except Exception as err:
        show_error(str(err))


if __name__ == "__main__":
    app = QApplication(sys.argv)

    filename = "birth_and_death_2017.csv"
    g_data, g_bd_years, g_bd_types = bd_stats.load_data(filename)

    # Главное окно приложения
    main_widget = QWidget()
    main_widget.resize(950, 430)
    main_widget.move(100, 100)
    main_widget.setWindowTitle("Рождаемость/смертность")
    main_widget.setWindowIcon(QIcon('main_icon.png'))

    # 1. Левая часть окна
    vbox_left = QVBoxLayout()

    # 1.1. Тип населения
    gb_type_options = QGroupBox("Население")
    gb_type_options.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)

    # Добавление типов населения
    lst_types = QListWidget()
    lst_types.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
    lst_types.addItem(CAPTION_ALL)
    for bd_type in g_bd_types:
        lst_types.addItem(bd_type)
    lst_types.setCurrentRow(0)
    lst_types.setFixedHeight(
        lst_types.sizeHintForRow(0) * (lst_types.count() + 1) +
        2 * lst_types.frameWidth()
    )
    lst_types.setFixedWidth(
        lst_types.sizeHintForColumn(0) + 2 * lst_types.frameWidth() + 50,
    )

    lst_types.currentItemChanged.connect(lambda _, x=lst_types:
                                         redraw_graph(widget=x))

    vbox_gb_type_options = QVBoxLayout()
    vbox_gb_type_options.addWidget(lst_types)
    gb_type_options.setLayout(vbox_gb_type_options)

    # 1.2. Годы
    gb_year_options = QGroupBox("Годы")
    gb_year_options.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)

    # Добавление лет анализа
    rd_years = []
    for bd_year in g_bd_years:
        rd_years.append(QCheckBox(str(bd_year)))
        rd_years[-1].setChecked(True)
        rd_years[-1].clicked.connect(lambda _, x=rd_years[-1]:
                                     redraw_graph(widget=x))

    grid_gb_year_options = QGridLayout()
    row, col = -1, 0
    for rd_year in rd_years:
        row += 1
        if row == len(rd_years) // 2 + 1:
            row = 0
            col += 1
        grid_gb_year_options.addWidget(rd_year, row, col)
    gb_year_options.setLayout(grid_gb_year_options)

    # 1.3. Левый виджет
    vbox_left.addWidget(gb_type_options)
    vbox_left.addWidget(gb_year_options)
    vbox_left.addStretch()

    # 2. Правая часть окна

    # Текст
    vbox_right = QVBoxLayout()

    gb_graph = QGroupBox("График")
    gb_graph.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
    vbox_gb_graph = QVBoxLayout()

    # Окно графика
    # Для экранов с высоким разрешением может понадобиться передать параметр
    # dpi, например, dpi=50
    fig, ax = plt.subplots()
    plt.xticks(rotation=45)
    plt.subplots_adjust(bottom=0.2, left=0.2)
    canvas = FigureCanvas(fig)
    canvas.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
    canvas.setMinimumWidth(700)
    canvas.updateGeometry()

    vbox_gb_graph.addWidget(canvas)
    gb_graph.setLayout(vbox_gb_graph)
    vbox_right.addWidget(gb_graph)

    # 3. Общее расположение элементов
    main_layout = QHBoxLayout()
    main_layout.setSpacing(15)
    main_layout.addLayout(vbox_left)
    main_layout.addLayout(vbox_right)

    # Запуск приложения
    redraw_graph(widget=main_widget)

    main_widget.setLayout(main_layout)
    main_widget.show()
    sys.exit(app.exec_())
